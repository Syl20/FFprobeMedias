#!/usr/bin/env python3
# -*-coding:Utf-8 -*
import os
import sys
import time
import signal

from functions import *
from FFprobeMedia import *
from configuration import *

signal.signal(signal.SIGTERM, exit_pyge)
signal.signal(signal.SIGINT, exit_pyge)

def main():
    log(0,' ------ START ------')

    log(0,'Scan ' + path_asset)
    for file in os.scandir(path_asset):
        if file.is_file() and extension(file.name) in list_extension:
            log(0,'Make folder ' + remove_extension(file.path))
            os.mkdir(remove_extension(file.path)) # Create folder with file_name without extension
            log(0,'Move {} to {}'.format(file.path, '{}/{}'.format(remove_extension(file.path),file.name)))
            os.rename(file.path, '{}/{}'.format(remove_extension(file.path),file.name)) # Move the file to this directory

    log(0,'Scan ' + path_asset)
    for root, dirs, files in os.walk(path_asset): # Tree
        for file_name in files: # For each file in tree
            file_path = os.path.join(root,file_name) # Get full path
            if os.path.isfile(file_path) and extension(file_path) in list_extension:
                log(0,'Create asset ' + file_path)
                FFprobeMedia(file_path) # Create FFprobeMedia instance

    #/!\ ERROR if no success assets...
    FFprobeMedia.recap()



if __name__ == '__main__':
    main()
