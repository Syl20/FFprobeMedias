#!/usr/bin/env python3
# -*-coding:Utf-8 -*


path_asset = '/Users/sgobron/Scripts/src/'
path_log = '/Users/sgobron/Scripts/FFprobeMedias/log/'

time_log = '%Y %b %d %H:%M:%S %Z'
log_levels = ['INFO','WARNING','ERROR','FATAL']

list_extension = ['mkv','mp4','ts','avi','mov']

dict_parameters = {
'video'    : ('index','codec_type','codec_name','width','height','resolution','pix_fmt','display_aspect_ratio','aspect_ratio','avg_frame_rate','bit_rate','field_order'),
'audio'    : ('index','codec_type','language','codec_name','channels','bit_rate'),
'subtitle' : ('index','codec_type','language','codec_name')}
