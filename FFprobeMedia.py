#!/usr/bin/env python3
# -*-coding:Utf-8 -*

import shutil
import sys
import os
import subprocess
import configparser
import time
from configuration import *
from functions import *
import operator


class StreamError(Exception):
    """Custom exception class"""

    def __init__(self, message='Error not specified'):
        self.message = message

    def __str__(self):
        return 'Stream Error : ' + self.message

class FFprobeMedia:

    dict_error = {}
    dict_asset = {}

    dict_analyze = {} # Dict of all parameters with dict of values and number of repeats
    # dict_analyze
    #   codec_name
    #       h264 : 1
    #       mpeg : 4
    #   resolution
    #       1920x1080 : 3
    #       1280x720 : 2

    def recap(cls):
        """Write a recap of all assets to a file"""

        le = max(len(x) for x in FFprobeMedia.dict_analyze.keys() or FFprobeMedia.dict_analyze.values()) + 3
        log(0,'Recap all assets in recap.log')
        with open(path_log + 'recap.log','w') as file:
            file.write(time.strftime(time_log) + '\n')

            for key, parameter in FFprobeMedia.dict_analyze.items():
                key = '[{}]'.format(key)
                file.write('\n\n{} {}'.format(key.ljust(le),len(parameter)))
                sorted_parameter = sorted(parameter.items(), key=operator.itemgetter(0), reverse = True )
                for value,count in sorted_parameter:
                    file.write('\n{} {}'.format(str(value).ljust(le),count))

            file.write('\n\nERROR : ' + str(len(FFprobeMedia.dict_error)))
            for asset in FFprobeMedia.dict_error.values():
                file.write('\n\t' + asset.file_path)

            file.write('\n\nSUCCESS : ' + str(len(FFprobeMedia.dict_asset)))
            for asset in FFprobeMedia.dict_asset.values():
                file.write('\n\t' + asset.file_path)
    recap = classmethod(recap)

    def __init__(self,file_path):

        if not os.path.isfile(file_path):
            log(2,'{} is not a file'.format(file_path))
            FFprobeMedia.dict_error[self.file_path] = self # Add this asset to error assets dict with file_path as key
            return 1

        self.file_path = file_path # Full path to the file
        self.file_dir = os.path.dirname(self.file_path) + '/'
        self.file_name = os.path.basename(self.file_path)
        self.file_extension = os.path.splitext(self.file_name)[1]
        self.file_size = os.path.getsize(file_path)

        self.log_FFprobe = self.file_path + '_FFprobe.log'
        self.log_analyze = self.file_path + '_analyze.log'

        self.format = ''
        self.duration = 0
        self.nb_streams = 0
        self.streams = []

        return_code = self.FFprobe() # Run FFprobe
        if return_code != 0: # FFprobe quit with errors
            shutil.move(self.log_FFprobe, path_log + self.file_name +'_error.log') # Move FFprobe logs to log folder
            log(2,'FFprobe quit with none 0 code. Please check ffprobe.log in' + path_log)
            FFprobeMedia.dict_error[self.file_path] = self # Add this asset to error assets dict with file_path as key
        else:
            os.remove(self.log_FFprobe) # Remove useless FFprobe logs
            log(0,'FFprobe OK.')

            return_code = self.parse() # Parse raw output of FFprobe
            if return_code != 0: # Parsing quit with errors
                log(2,'Parsing quit with errors. Asset add to error assets.')
                FFprobeMedia.dict_error[self.file_path] = self # Add this asset to error assets dict with file_path as key
            else:
                log(0,'Parsing OK.')

                self.log_parameters() # Write all parameters in a file
                FFprobeMedia.dict_asset[self.file_path] = self # Add this asset to success assets dict with file_path as key

                self._count('size',self.file_size)
                self._count('extension',self.file_extension)
                self._count('format',self.format)
                self._count('duration',str(self.duration))
                self._count('nb_streams',str(self.nb_streams))

    # print(self.get_index('display_aspect_ratio','16\:9'))


    def __str__(self):
        return self.file_name

    def FFprobe(self):
        stdout = open(self.log_analyze,'w')
        stderr = open(self.log_FFprobe,'w')

        # Launch FFprobe subprocess
        process = subprocess.Popen( \
        'ffprobe -hide_banner -print_format ini -show_streams -show_format ' + '\"' + self.file_path + '\"',
        shell = True, stdout = stdout, stderr = stderr)
        return_code = process.wait() # Wait for process to finish

        stdout.close()
        stderr.close()
        return return_code

    def parse(self):
        parser = configparser.ConfigParser(interpolation=configparser.BasicInterpolation()) # Create parser object
        try:
            log(0,'Open and parse FFprobe raw output.')
            open(self.log_analyze) # Check if possible to open
            parser.read(self.log_analyze) # Parse file

            self.format = parser['format']['format_name']
            self.duration = second2hms(parser.getfloat('format','duration'))
            self.nb_streams = parser.getint('format','nb_streams')
            log(0,str(self.nb_streams) + ' streams detected.')

        except IOError as error :
            log(2,'IO Error : ' + str(error))
            return 1
        except KeyError as error:
            log(2,'Key Error : Key not found ' + str(error))
            return 1

        for stream_index in range(0,self.nb_streams): # For all streams index
            try:
                stream_type = parser['streams.stream.' + str(stream_index)]['codec_type'] # Get the type of stream
            except KeyError as error:
                log(2,'Key not found : ' + str(error))
                return 1

            if stream_type in configuration.dict_parameters.keys():
                stream = {} # Temporary dict for stream

                try:
                    for parameter in configuration.dict_parameters[stream_type] : # For all parameters defined in list with stream_type as key
                        if parameter == 'language': # language is in tag section
                            value = parser['streams.stream.{}.tags'.format(str(stream_index))][parameter]
                        elif parameter == 'resolution': # Create resolution from width and height
                            value = '{}x{}'.format(stream['width'],stream['height'])
                        elif parameter == 'aspect_ratio': # Calculate resolution from width and height
                            value = round(int(stream['width'])/int(stream['height']),3)
                        else: # normal case
                            value = parser['streams.stream.{}'.format(str(stream_index))][parameter] # Get parameter
                        stream[parameter] = value
                        self._count(parameter,value)
                except KeyError as error:
                    log(1,'Key not found ' + str(error))

                self.streams.append(stream) # Add the stream to the general streams dict
        return 0

    def _count(self,parameter,value):
        """Increase number of repeats of this parameter or create if not in dict yet"""

        if parameter in FFprobeMedia.dict_analyze.keys() and value in FFprobeMedia.dict_analyze[parameter].keys():
            FFprobeMedia.dict_analyze[parameter][value] += 1 # Increase
        elif parameter in FFprobeMedia.dict_analyze.keys():
            FFprobeMedia.dict_analyze[parameter][value] = 1 # Set this value to 1
        else:
            FFprobeMedia.dict_analyze[parameter] = {} # Create dict for this parameter
            FFprobeMedia.dict_analyze[parameter][value] = 1 # Set this value to 1

    def get_index(self, parameter, value):
        """Retun a list of index of streams that match parameter = value"""
        list_match = []
        for stream in self.streams:
            try:
                if stream[parameter] == value:
                    list_match.append(self.streams.index(stream))
                else:
                    pass
            except KeyError:
                pass

        return list_match

    def log_parameters(self):
        """Write all parameters and value to file """

        log(0,'Write parameters to ' + self.log_analyze)
        with open(self.log_analyze,'w') as log_file :
            log_file.write(time.strftime("%Y %b %d %H:%M:%S %Z") + '\n\n')
            log_file.write(str(self.duration) + '\n\n')

            log_file.write('{}\n[{}] {}'.format(self.file_name,self.format,self.file_size))

            for stream in self.streams:
                log_file.write('\n\t[{}] {}'.format(stream['index'],stream['codec_type']))
                for key,value in stream.items():
                    log_file.write('\n\t\t{} = {}'.format(key,value))

if __name__ == '__main__':
    asset1 = FFprobeMedia("/Users/sgobron/Scripts/src/asie.ts")
    try:
        raise StreamError('Unknow stream type')
    except Exception as error:
        print(error)
