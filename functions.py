#!/usr/bin/env python3
# -*-coding:Utf-8 -*

import sys
import os
import time
import configuration
import math

def exit_pyge(signal,frame,exit_code=1):
    """Print message and exit program."""

    if exit_code == 0:
        print("Exit " + str(exit_code))
    else:
        print("Exit " + str(exit_code) + ". Please check logs : " + configuration.path_log)
    sys.exit(exit_code)


def log(log_level, log_details, log_path=configuration.path_log):
    """Format, add timestamp and write log_level and
        log_details in the log file define by log_path."""

    log_level = configuration.log_levels[log_level]

    try:
        with open(log_path + "run.log", "a") as log_file:
            log_file.write(time.strftime(configuration.time_log) + " > " + "[" + log_level + "]" + " > " + log_details + "\n")
    except Exception as error_log:
        sys.exit('Exit 2 : Error on log file > ' + str(error_log))


def extension(file_path):
    return file_path.split(os.extsep)[-1]

def remove_extension(file_path):
    return '.'.join(file_path.split('.')[:-1])


def bytes2human(n, format='%(value).1f %(symbol)s', symbols='customary'):
    """
    Convert n bytes into a human readable string based on format.
    symbols can be either "customary", "customary_ext", "iec" or "iec_ext",
    """

    SYMBOLS = {
    'customary'     : ('B', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'),
    'customary_ext' : ('byte', 'kilo', 'mega', 'giga', 'tera', 'peta', 'exa',
                       'zetta', 'iotta'),
    'iec'           : ('Bi', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi'),
    'iec_ext'       : ('byte', 'kibi', 'mebi', 'gibi', 'tebi', 'pebi', 'exbi',
                       'zebi', 'yobi')}

    n = int(n)
    if n < 0:
        raise ValueError("n < 0")
    symbols = SYMBOLS[symbols]
    prefix = {}
    for i, s in enumerate(symbols[1:]):
        prefix[s] = 1 << (i+1)*10
    for symbol in reversed(symbols[1:]):
        if n >= prefix[symbol]:
            value = float(n) / prefix[symbol]
            return format % locals()
    return format % dict(symbol=symbols[0], value=n)

def second2hms(n):
    return time.strftime('%H:%M:%S',time.gmtime(n))
